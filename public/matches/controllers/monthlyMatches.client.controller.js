/**
 * Created by Jack on 1/11/17.
 */
angular.module('matches').controller('MonthlyMatchesCtrl', ['$scope', 'MonthlyMatchSvc',
	function ($scope, MonthlyMatchSvc) {
		$scope.monthlyMatches = MonthlyMatchSvc.query();
	}
]);