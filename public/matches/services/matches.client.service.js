/**
 * Created by Jack on 1/11/17.
 */
angular.module('matches').factory('MonthlyMatchSvc', ['$resource',
	function ($resource) {
		return $resource('api/matches/monthly');
	}
]);