/**
 * Created by Jack on 9/30/16.
 */
angular.module('articles').config(['$stateProvider',
    function ($stateProvider) {
        $stateProvider
            .state('articles', {
                url: '/articles',
                views: {
                    'headerSection': {
                        templateUrl: 'authentication/views/login.client.view.html'
                    },
                    'articleSectionOne': {
                        templateUrl: 'articles/views/listArticles.client.view.html'
                    },
	                'articleSectionTwo': {
		                template: '<h1>This is the Article section 2</h1><br><p>{{ articles }}</p>'
	                }
                }
            })
            .state('articleCreate', {
                url: '/articles/create',
                views: {
                    'headerSection': {
                        templateUrl: 'authentication/views/login.client.view.html'
                    },
                    'articleSectionOne': {
                        templateUrl: 'articles/views/createArticle.client.view.html'
                    },
                    'articleSectionTwo': {
                        template: '<h1>This is the Article section 2</h1><br><p>{{ articles }}</p>'
                    }
                }
            })
            .state('articleView', {
                url: '/articles/:articleId',
                views: {
                    'headerSection': {
                        templateUrl: 'authentication/views/login.client.view.html'
                    },
                    'articleSectionOne': {
                        templateUrl: 'articles/views/viewArticle.client.view.html'
                    },
	                'articleSectionTwo': {
		                template: '<h1>This is the Article section 2</h1>'
	                }
                }
            })
            .state('articleEdit', {
                url: '/articles/:articleId/edit',
                views: {
                    'headerSection': {
                        templateUrl: 'authentication/views/login.client.view.html'
                    },
                    'articleSectionOne': {
                        templateUrl: 'articles/views/editArticle.client.view.html'
                    },
	                'articleSectionTwo': {
		                template: '<h1>This is the Article section 2</h1>'
	                }
                }
            });
    }
]);