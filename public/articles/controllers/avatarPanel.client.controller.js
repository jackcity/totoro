/**
 * Created by office on 2016-10-14.
 */
angular.module('articles').controller('AvatarPanelCtrl', ['$scope', '$mdPanel',
    function ($scope, $mdPanel) {
        $scope._mdPanel = $mdPanel;
        $scope.avatarImages = [
            '/img/avatarImg/avatar01.jpeg',
            '/img/avatarImg/avatar02.jpeg',
            '/img/avatarImg/avatar03.jpeg',
            '/img/avatarImg/avatar04.jpeg',
            '/img/avatarImg/avatar05.jpeg',
            '/img/avatarImg/avatar06.jpeg',
            '/img/avatarImg/avatar07.jpeg',
            '/img/avatarImg/avatar08.jpeg',
            '/img/avatarImg/avatar09.jpeg',
            '/img/avatarImg/avatar10.jpeg',
            '/img/avatarImg/avatar11.jpeg',
            '/img/avatarImg/avatar12.jpeg',
            '/img/avatarImg/avatar13.jpeg',
            '/img/avatarImg/avatar14.jpeg',
            '/img/avatarImg/avatar15.jpeg',
            '/img/avatarImg/avatar16.jpeg'
        ];
        $scope.selected = { favoriteAvatar: 'Donut' };
        $scope.disableParentScroll = false;
    }
]);

// angular.module('articles').controller('AvatarPanelCtrl', ['$scope', '$maPanel',
//     function ($scope, $mdPanel) {
//
//

//
//
//
//         avatarPanelCtrl.prototype.showMenu = function(ev) {
//             var position = this._mdPanel.newPanelPosition()
//                 .relativeTo('.avatar-menu-open-button')
//                 .addPanelPosition(this._mdPanel.xPosition.ALIGN_START, this._mdPanel.yPosition.BELOW);
//
//             var config = {
//                 attachTo: angular.element(document.body),
//                 controller: PanelMenuCtrl,
//                 controllerAs: 'panelCtrl',
//                 templateUrl: 'articles/views/avatarPanel.client.view.html',
//                 panelClass: 'avatar-menu-example',
//                 position: position,
//                 locals: {
//                     'selected': this.selected,
//                     'avatarImages': this.avatarImages
//                 },
//                 openFrom: ev,
//                 clickOutsideToClose: true,
//                 escapeToClose: true,
//                 focusOnOpen: false,
//                 zIndex: 2
//             };
//
//             this._mdPanel.open(config);
//         };
//
//         function PanelMenuCtrl(mdPanelRef, $timeout) {
//             this._mdPanelRef = mdPanelRef;
//             this.favoriteAvatar = this.selected.favoriteAvatar;
//             $timeout(function () {
//                 var selected = document.querySelector('.avatar-menu-item.selected');
//                 if (selected) {
//                     angular.element(selected).focus();
//                 } else {
//                     angular.element(document.querySelectorAll('.avatar-menu-item')[0]).focus();
//                 }
//             });
//         }
//
//         PanelMenuCtrl.prototype.selectAvatar = function(avatar) {
//             this.selected.favoriteAvatar = avatar;
//             this._mdPanelRef && this._mdPanelRef.close().then(function() {
//                 angular.element(document.querySelector('.avatar-menu-open-button')).focus();
//             });
//         };
//     }
// ]);



// The previous codes
// angular.module('articles').controller('AvatarPanelCtrl', AvatarPanelCtrl);
//
// function AvatarPanelCtrl($mdPanel) {
//     this._mdPanel = $mdPanel;
//     this.avatarImages = [
//         '/img/avatarImg/avatar01.jpeg',
//         '/img/avatarImg/avatar02.jpeg',
//         '/img/avatarImg/avatar03.jpeg',
//         '/img/avatarImg/avatar04.jpeg',
//         '/img/avatarImg/avatar05.jpeg',
//         '/img/avatarImg/avatar06.jpeg',
//         '/img/avatarImg/avatar07.jpeg',
//         '/img/avatarImg/avatar08.jpeg',
//         '/img/avatarImg/avatar09.jpeg',
//         '/img/avatarImg/avatar10.jpeg',
//         '/img/avatarImg/avatar11.jpeg',
//         '/img/avatarImg/avatar12.jpeg',
//         '/img/avatarImg/avatar13.jpeg',
//         '/img/avatarImg/avatar14.jpeg',
//         '/img/avatarImg/avatar15.jpeg',
//         '/img/avatarImg/avatar16.jpeg'
//     ];
//     this.selected = { favoriteAvatar: 'Donut' };
//     this.disableParentScroll = false;
// }
//
// AvatarPanelCtrl.prototype.showMenu = function(ev) {
//     var position = this._mdPanel.newPanelPosition()
//         .relativeTo('.avatar-menu-open-button')
//         .addPanelPosition(this._mdPanel.xPosition.ALIGN_START, this._mdPanel.yPosition.BELOW);
//
//     var config = {
//         attachTo: angular.element(document.body),
//         controller: PanelMenuCtrl,
//         controllerAs: 'panelCtrl',
//         templateUrl: 'articles/views/avatarPanel.client.view.html',
//         panelClass: 'avatar-menu-example',
//         position: position,
//         locals: {
//             'selected': this.selected,
//             'avatarImages': this.avatarImages
//         },
//         openFrom: ev,
//         clickOutsideToClose: true,
//         escapeToClose: true,
//         focusOnOpen: false,
//         zIndex: 2
//     };
//
//     this._mdPanel.open(config);
// };
//
// function PanelMenuCtrl(mdPanelRef, $timeout) {
//     this._mdPanelRef = mdPanelRef;
//     this.favoriteAvatar = this.selected.favoriteAvatar;
//     $timeout(function () {
//         var selected = document.querySelector('.avatar-menu-item.selected');
//         if (selected) {
//             angular.element(selected).focus();
//         } else {
//             angular.element(document.querySelectorAll('.avatar-menu-item')[0]).focus();
//         }
//     });
// }
//
// PanelMenuCtrl.prototype.selectAvatar = function(avatar) {
//     this.selected.favoriteAvatar = avatar;
//     this._mdPanelRef && this._mdPanelRef.close().then(function() {
//         angular.element(document.querySelector('.avatar-menu-open-button')).focus();
//     });
// };