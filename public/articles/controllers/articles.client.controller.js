/**
 * Created by Jack on 9/30/16.
 */
angular.module('articles').controller('ArticlesCtrl', [
    '$scope', '$state', '$stateParams', 'Upload', '$timeout', 'AuthenticationSvc', 'ArticlesSvc',
    function ($scope, $state, $stateParams, Upload, $timeout, AuthenticationSvc, ArticlesSvc) {
        $scope.authentication = AuthenticationSvc;

        // $scope.testAlert = "x = {'y':''.constructor.prototype}; x['y'].charAt=[].join;$eval('x=alert(1)');";
        // $scope.avatarImages = [
        //     '/img/avatarImg/avatar01.jpeg',
        //     '/img/avatarImg/avatar02.jpeg',
        //     '/img/avatarImg/avatar03.jpeg',
        //     '/img/avatarImg/avatar04.jpeg',
        //     '/img/avatarImg/avatar05.jpeg',
        //     '/img/avatarImg/avatar06.jpeg',
        //     '/img/avatarImg/avatar07.jpeg',
        //     '/img/avatarImg/avatar08.jpeg',
        //     '/img/avatarImg/avatar09.jpeg',
        //     '/img/avatarImg/avatar10.jpeg',
        //     '/img/avatarImg/avatar11.jpeg',
        //     '/img/avatarImg/avatar12.jpeg',
        //     '/img/avatarImg/avatar13.jpeg',
        //     '/img/avatarImg/avatar14.jpeg',
        //     '/img/avatarImg/avatar15.jpeg',
        //     '/img/avatarImg/avatar16.jpeg'
        // ];

        $scope.requiresLogin = function () {
        	if (!AuthenticationSvc.user) {
        		$state.go('articles').then(function () {
        			alert('로그인을 먼저 해주세요.');
        		});
	        }
        };

        $scope.uploadPic = function(file) {
            file.upload = Upload.upload({
                url: '/api/articles/upload',
                data: { username: $scope.username, file: file }
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        };

        $scope.create = function () {
            var article = new ArticlesSvc({
                title: this.title,
                content: this.content,
                file: this.picFile.name
            });

            article.$save(function (response) {
                $state.go('articleView', {
                    articleId: response._id
                });
                // $scope.postResponse = response;
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.find = function () {
            $scope.articles = ArticlesSvc.query();
        };

        $scope.findOne = function () {
            $scope.article = ArticlesSvc.get({
                articleId: $stateParams.articleId
            });
        };

        $scope.update = function () {
            $scope.article.$update(function () {
                $state.go('articleView', {
                    articleId: $scope.article._id
                });
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.delete = function (article) {
            if (article) {
                article.$remove(function () {
                    for (var i in $scope.articles) {
                        if ($scope.articles[i] === article) {
                            $scope.articles.splice(i, 1);
                        }
                    }
                });
            } else {
                $scope.article.$remove(function () {
                    $state.go('articles');
                });
            }
        };
    }
]);