/**
 * Created by Jack on 9/30/16.
 */
angular.module('articles').factory('ArticlesSvc', ['$resource',
    function ($resource) {
        return $resource('api/articles/:articleId', {
            articleId: '@_id'
        }, {
            update: { method: 'PUT' }
        });
    }
]);