/**
 * Created by Jack on 1/6/17.
 */
angular.module('authentication').controller('SignupCtrl', ['$scope', function ($scope) {
	$scope.userRoles = [
		{ kr: '일반 사용자', en: 'User'},
		{ kr: '관리자', en: 'Admin'}
	];
}]);