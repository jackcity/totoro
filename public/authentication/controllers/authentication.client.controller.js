/**
 * Created by Jack on 9/27/16.
 */
angular.module('authentication').controller('AuthenticationCtrl', ['$scope', 'AuthenticationSvc',
    function ($scope, AuthenticationSvc) {
        $scope.name = AuthenticationSvc.user ? AuthenticationSvc.user.fullName : 'my neighbor TOTOro';
        $scope.coin = AuthenticationSvc.user ?
            (AuthenticationSvc.user.coin > 0 ? AuthenticationSvc.user.coin : 0) : 0;
        $scope.userRole = AuthenticationSvc.user ?
	        (AuthenticationSvc.user.role === 'Admin' ? true : false) : null;
    }
]);