/**
 * Created by Jack on 9/29/16.
 */
angular.module('authentication').factory('AuthenticationSvc', [
    function () {
        this.user = window.user;

        return { user: this.user };
    }
]);