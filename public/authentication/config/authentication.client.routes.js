/**
 * Created by Jack on 9/28/16.
 */
angular.module('authentication').config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('home', {
                url: '/',
                views: {
                    'headerSection': {
                        templateUrl: 'authentication/views/login.client.view.html'
                    },
                    'articleSectionOne': {
                        template: '<h1>This is the Article section 1</h1>'
                    },
                    'articleSectionTwo': {
                        templateUrl: 'matches/views/monthlyMatchList.client.view.html'
                    }
                }
            })
            .state('signup', {
                url: '/signup',
                views: {
                    'headerSection': {
                        templateUrl: 'authentication/views/login.client.view.html'
                    },
                    'articleSectionOne': {
                        templateUrl: 'authentication/views/signup.client.view.html'
                    },
	                'articleSectionTwo': {
		                templateUrl: 'matches/views/monthlyMatchList.client.view.html'
	                }
                }
            });
    }
]);