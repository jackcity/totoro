/**
 * Created by Jack on 1/6/17.
 */
angular.module('admin').config(['$stateProvider', '$urlRouterProvider',
	function ($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/');
		
		$stateProvider
			.state('admin', {
				url: '/admin',
				views: {
					'headerSection': {
						templateUrl: 'authentication/views/login.client.view.html'
					},
					'articleSectionOne': {
						template: '<h1>This is the Article section 1</h1>'
					},
					'articleSectionTwo': {
						templateUrl: 'admin/views/matchListEditor.client.view.html'
					}
				}
			});
	}
]);