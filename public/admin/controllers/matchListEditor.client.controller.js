/**
 * Created by Jack on 1/6/17.
 */
angular.module('admin').controller('MatchListEditorCtrl', ['$scope', '$state', '$resource', 'AuthenticationSvc',
	function ($scope, $state, $resource, AuthenticationSvc) {
		$scope.requiresLogin = function () {
			if (!AuthenticationSvc.user) {
				$state.go('home').then(function () {
					alert('로그인을 먼저 해주세요.');
				});
			} else {
				if (AuthenticationSvc.user.role !== 'Admin') {
					$state.go('home').then(function () {
						alert('서비스 관리자가 아닙니다.');
					});
				}
			}
		};
		$scope.seasons = ['2016/17시즌', '2017/18시즌'];
		$scope.years = [2016, 2017, 2018];
		$scope.months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
		
		$scope.removeMatchResource = $resource('admin/match/remove');
		$scope.removeMatchForm = function () {
			$scope.removeMatchResource.save($scope.removeMatch, function (response) {
				$scope.removedMatchNumber = response.n;
				$scope.removeForm.$setPristine();
			}, function (errResponse) {
				$scope.errorMessage = errResponse.data.message;
			});
		};
		
		$scope.saveMatchResource = $resource('admin/match/save');
		$scope.saveMatchForm = function () {
			$scope.saveMatchResource.save($scope.saveMatch, function (response) {
				$scope.savedMatchResponse = response;
				$scope.saveForm.$setPristine();
			}, function (errResponse) {
				$scope.errorMessage = errResponse.data.message;
			});
		};
	}
]);