/**
 * Created by office on 2016-10-19.
 */
angular.module('charging').config(['$stateProvider',
    function ($stateProvider) {
        $stateProvider
            .state('charging', {
                url: '/payment',
                views: {
                    'headerSection': {
                        templateUrl: 'authentication/views/login.client.view.html'
                    },
                    'articleSectionOne': {
                        template: '<h1>This is the Article section 1</h1>'
                    },
                    'articleSectionTwo': {
                        templateUrl: 'charging/views/charging.client.view.html'
                    }
                }
            });
    }
]);