/**
 * Created by office on 2016-10-27.
 */
angular.module('charging').factory('PaymentSvc', ['$resource',
    function ($resource) {
        return $resource('api/payment/:paymentId', {
            paymentId: '@_id'
        }, {
            update: { method: 'PUT' }
        });
    }
]);