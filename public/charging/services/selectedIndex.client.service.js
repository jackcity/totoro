/**
 * Created by Jack on 10/25/16.
 */
angular.module('charging').factory('SelectedIndexSvc', [function () {
	var selectChargingPriceIndex = '',
        selectPaymentWayIndex = '',
        selectCardCompanyIndex = '',
        selectBankIndex = '',
		selectedChargingPriceIndex = '',
		selectedPaymentWayIndex = '',
        selectedCardCompanyIndex = '',
        selectedBankIndex = '';
	
	return {
        selectChargingPriceIndex: function (index) {
	        return selectChargingPriceIndex = index;
        },
		selectPaymentWayIndex: function (index) {
			return selectPaymentWayIndex = index;
		},
        selectCardCompanyIndex: function (index) {
            return selectCardCompanyIndex = index;
        },
        selectBankIndex: function (index) {
            return selectBankIndex = index;
        },
        getChargingPrice: function () {
            return selectedChargingPriceIndex = selectChargingPriceIndex;
        },
		getPaymentWayIndex: function () {
			return selectedPaymentWayIndex = selectPaymentWayIndex;
		},
		getCardCompanyIndex: function () {
		    return selectedCardCompanyIndex = selectCardCompanyIndex;
        },
        getBankIndex: function () {
            return selectedBankIndex = selectBankIndex;
        }
	};
}]);