/**
 * Created by Jack on 10/20/16.
 */
angular.module('charging').controller('ChargingCtrl', [
    '$rootScope', '$scope', '$state', 'AuthenticationSvc', 'SelectedIndexSvc',
    function($rootScope, $scope, $state, AuthenticationSvc, SelectedIndexSvc) {
		var vm = this;

		$scope.requiresLogin = function () {
			if (!AuthenticationSvc.user) {
				$state.go('home').then(function () {
					alert('로그인을 먼저 해주세요.');
				});
			}
		};

        $scope.selectChargingPrice = function (index) {
            SelectedIndexSvc.selectChargingPriceIndex(index);
        };
	    $scope.selectPaymentWay = function (index) {
		    SelectedIndexSvc.selectPaymentWayIndex(index);
	    };
	    $scope.selectCardCompany = function (index) {
	        SelectedIndexSvc.selectCardCompanyIndex(vm.cardCompanies[index]);
        };
        $scope.selectBank = function (index) {
            SelectedIndexSvc.selectBankIndex(vm.banks[index]);
        };

		vm.chargingPrices = [{
			id: 1,
			cost: 10000,
			chargingCoin: 10000,
			mileage: 0
		}, {
			id: 2,
			cost: 50000,
			chargingCoin: 50000,
			mileage: 5000
		}, {
			id: 3,
			cost: 100000,
			chargingCoin: 100000,
			mileage: 11000
		}, {
			id: 4,
			cost: 500000,
			chargingCoin: 500000,
			mileage: 60000
		}, {
			id: 5,
			cost: 1000000,
			chargingCoin: 1000000,
			mileage: 150000
		}];
		vm.cardCompanies = [
		    '신한카드', '국민카드', '삼성카드', 'BC카드/우리카드', '현대카드',
            '롯데카드', '씨티카드', 'NH카드', '외환카드', '하나SK카드'
        ];
        vm.banks = [
            '신한은행', '국민은행', '우리은행', '기업은행', '하나은행',
            '농협', '외환은행', 'SC제일은행'
        ];

	}
]);