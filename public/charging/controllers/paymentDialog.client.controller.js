/**
 * Created by Jack on 10/25/16.
 */
angular.module('charging')
	.controller('PaymentDialogCtrl', ['$mdPanel', PaymentDialogCtrl])
	.controller('PanelDialogCtrl', [
		'$scope', '$state', '$window', 'SelectedIndexSvc', 'PaymentSvc', 'mdPanelRef', PanelDialogCtrl]);

function PaymentDialogCtrl($mdPanel) {
	this._mdPanel = $mdPanel;
}

PaymentDialogCtrl.prototype.showDialog = function() {
	var position = this._mdPanel.newPanelPosition()
		.absolute()
		.center();
	
	var config = {
		attachTo: angular.element(document.body),
		controller: PanelDialogCtrl,
		controllerAs: 'ctrl',
		disableParentScroll: this.disableParentScroll,
		templateUrl: 'paymentDialog.client.view.html',
		hasBackdrop: true,
		panelClass: 'payment-dialog-example',
		position: position,
		trapFocus: true,
		zIndex: 150,
		clickOutsideToClose: true,
		escapeToClose: true,
		focusOnOpen: true
	};
	
	this._mdPanel.open(config);
};

function PanelDialogCtrl($scope, $state, $window, SelectedIndexSvc, PaymentSvc, mdPanelRef) {
	this._mdPanelRef = mdPanelRef;
    this.chargingPrice = SelectedIndexSvc.getChargingPrice();
	this.paymentWay = SelectedIndexSvc.getPaymentWayIndex();
	this.cardCompany = SelectedIndexSvc.getCardCompanyIndex();
	this.bank = SelectedIndexSvc.getBankIndex();

    this.cardNumbers = [];
    this.cardCVC = '';
    this.accountNumber = '';
    this.accountPwd = '';
    this.sendPaymentInfo = function () {
    	if (this.paymentWay === '일반결제') {
		    var paymentInfo = new PaymentSvc({
			    payCost: this.chargingPrice,
			    cardCompany: this.cardCompany,
			    cardNumbers: this.cardNumbers,
			    cardCVC: this.cardCVC,
			    paid: false
		    });
	    } else if (this.paymentWay === '계좌이체') {
		    var paymentInfo = new PaymentSvc({
			    payCost: this.chargingPrice,
			    bank: this.bank,
			    accountNumber: this.accountNumber,
			    accountPwd: this.accountPwd,
			    paid: false
		    });
	    }

		paymentInfo.$save(function (response) {
		    //$scope.successResponse = response;
			mdPanelRef.close()
				.then(function () {
					$window.location.reload();
				})
				.then(function () {
					alert('결제되었습니다.');
				});
        }, function (errorResponse) {
            $scope.error = errorResponse.data.message;
        });
    };
}

PanelDialogCtrl.prototype.closeDialog = function() {
	var panelRef = this._mdPanelRef;
	
	panelRef && panelRef.close().then(function() {
		angular.element(document.querySelector('.payment-dialog-open-button')).focus();
		panelRef.destroy();
	});
};
