/**
 * Created by Jack on 9/27/16.
 */
var mainApplicationModuleName = 'totoro';
var mainApplicationModule = angular.module(mainApplicationModuleName, [
    'ngMaterial', 'ngMessages', 'ngResource', 'ui.router', 'ngFileUpload', 'md.data.table',
	'authentication', 'articles', 'charging', 'matches', 'admin'
]);

mainApplicationModule.config(['$locationProvider',
    function ($locationProvider) {
        $locationProvider.hashPrefix('!');
    }
]);

angular.element(document).ready(function () {
    angular.bootstrap(document, [mainApplicationModuleName]);
});