# README #

* 단순 코드 리뷰를 위해 공개하는 것입니다.
* 서버와 클라이언트에 쓰인 module/library 등은 push하지 않았습니다.
* 모든것의 구현이 끝난것이 아니라 일부 기능하지 않습니다.

학원의 정보보안 과정에서 했던 프로젝트의 일부로서 데이터의 보안 테스트에 초점이 맞추어진 서비스입니다.  
스포츠토토라는 주제를 가지고 클라이언트와 서버간 Ajax 등의 데이터 패킷이 프로토콜에 따라 어떤 보안 이슈가 있을 수 있는지에 관해서와 AngularJs에서의 XSS 공격 등에 대해 테스트해 보았습니다.  
Back-end : Node.js, MongoDB  
Front-end : AngularJS

간단한 로컬 로그인과 코인의 결제기능, 타 사이트로부터의 데이터를 Scraping하여 본 서비스에서 사용할 수 있도록 구현하였습니다.  
Scraping을 위해 request, cheerio, phantom, casper 등을 이용하여 테스트해 보았지만 최종적으로 속도가 빠르고 서버와의 궁합이 좋은 nightmare를 활용하였습니다.