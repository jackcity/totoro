/**
 * Created by Jack on 9/19/16.
 */
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var mongoose = require('./config/mongoose.js'),
    express = require('./config/express.js'),
    passport = require('./config/passport.js');

var db = mongoose(),
    app = express(),
    passport = passport(),
    port = process.env.PORT || 3000;

app.listen(port);

module.exports = app;

console.log('Server running at http://localhost:' + port + '/');