/**
 * Created by Jack on 9/22/16.
 */
var config = require('./config.js'),
    mongoose = require('mongoose');

mongoose.Promise = global.Promise;

module.exports = function () {
    var db = mongoose.connect(config.dbUri);

    require('../app/models/user.server.model.js');
    require('../app/models/article.server.model.js');
    require('../app/models/match.server.model.js');
    require('../app/models/bet.server.model.js');

    return db;
};