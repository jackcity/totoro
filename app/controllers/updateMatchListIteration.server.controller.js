/**
 * Created by Jack on 12/18/16.
 */
// Export this to be used for a router for an administrator later.
var spawn = require('child_process').spawn;
var loop = function () {
	var child = spawn('node', ['../scrapers/updateCurrentMatchList.server.nightmare.js']);
	
	// child.stdout.setEncoding('utf8');
	child.stdout.on('data', function (data) {
		// console.log('stdout (updated to DB) : ' + data);
		console.log('stdout (found from DB) : ' + data);
	});
	child.stderr.on('data', function (err) {
		console.log('stderr : ' + err);
	});
	child.on('exit', function (code) {
		console.log('child process exited with code : ' + code);
	});
};

setInterval(loop, 10000);