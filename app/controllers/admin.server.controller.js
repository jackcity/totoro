/**
 * Created by Jack on 1/9/17.
 */
require('../models/match.server.model.js');

var mongoose = require('mongoose'),
	Match = mongoose.model('Match'),
	spawn = require('child_process').spawn;

exports.removeMatch = function (req, res) {
	var	season = req.body.season,
		month = req.body.month,
		seasonQuery = {},
		monthQuery = {};
	
	seasonQuery['season'] = season;
	monthQuery['month'] = month;

	Match.find(seasonQuery).remove(monthQuery).exec(function (err, result) {
		if (err) {
			return console.log(err);
		} else {
			res.json(result);
		}
	});
};

exports.saveMatch = function (req, res) {
	var year = req.body.year,
		month = req.body.month,
		child = spawn('node', ['./app/scrapers/saveMonthlyMatchList.server.nightmare.js']);
	
	exports.year = year;
	exports.month = month;
	
	child.stdout.setEncoding('utf8');
	child.stdout.on('data', function (data) {
		console.log('stdout : ' + data);
		res.send(data);
	});
	child.stderr.on('data', function (err) {
		console.log('stderr : ' + err);
	});
	child.on('exit', function (code) {
		console.log('child process exited with code : ' + code);
	});
};