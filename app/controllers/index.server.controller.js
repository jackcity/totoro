/**
 * Created by Jack on 9/19/16.
 */
exports.render = function (req, res) {
    var date = new Date();

    if (req.session.visitTime) {
        console.log('visited at : ' + req.session.visitTime);
    } else {
        req.session.visitTime = date;
        console.log('visited at : ' + req.session.visitTime);
    }
    //console.log(req.session);
    //console.log('This is the req.user : \n' + req.user);

    res.render('index', {
        title: '토토로',
        user: JSON.stringify(req.user),
        name: req.user ? req.user.fullName : '고객'
    });
};