/**
 * Created by office on 2016-10-28.
 */
var getErrorMessage = function (err) {
	if (err.errors) {
		for (var errName in err.errors) {
			if (err.errors[errName].message) return err.errors[errName].message;
		}
	} else {
		return '알 수 없는 서버 에러';
	}
};

exports.acceptPayment = function (req, res) {
    var user = req.user;
    var paymentResponse = {
        userCoin: req.user.coin
    };

    paymentResponse.userCoin += req.body.payCost;
	paymentResponse.paid = true;
	user.coin = paymentResponse.userCoin;

    user.save(function (err) {
	    if (err) {
		    return res.status(400).send({ message: getErrorMessage(err) });
	    } else {
            res.json(paymentResponse);
	    }
    });
};