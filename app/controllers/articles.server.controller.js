/**
 * Created by Jack on 9/29/16.
 */
var mongoose = require('mongoose'),
    Article = mongoose.model('Article'),
	formidable = require('formidable'),
	fs = require('fs-extra');

var getErrorMessage = function (err) {
    if (err.errors) {
        for (var errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].message;
        }
    } else {
        return '알 수 없는 서버 에러';
    }
};

exports.uploadTest = function (req, res) {
	var form = new formidable.IncomingForm();

	form.encoding = 'utf-8';
	form.keepExtensions = true;
	form.parse(req, function (err, fields, files) {});
    form.on('end', function (fields, files) {
        for (var i = 0; i < this.openedFiles.length; i++) {
            var temp_path = this.openedFiles[i].path,
                file_name = this.openedFiles[i].name;
            var new_location = 'public/uploadFiles/';

            // console.log("temp_path : ", temp_path);
            // console.log("file_name : ", file_name);
            // console.log(this.openedFiles[i]);

            fs.copy(temp_path, new_location + file_name, function (err) {
                if (err) console.log(err);
            });

            // var article = new Article();
            // article.file = temp_path;
            // console.log(article);
        }
    });
};

exports.create = function (req, res) {
    var article = new Article(req.body);
    article.creator = req.user;

    console.log('rea.body : ');
    console.log(req.body);

    article.save(function (err) {
        if (err) {
            return res.status(400).send({ message: getErrorMessage(err) });
        } else {
            res.json(article);
        }
    });
};

exports.list = function (req, res) {
    Article.find().sort('-created').populate('creator', 'fullName')
        .exec(function (err, articles) {
            if (err) {
                return res.status(400).send({ message: getErrorMessage(err) });
            } else {
                res.json(articles);
            }
        });
};

exports.articleByID = function (req, res, next, id) {
    Article.findById(id).populate('creator', 'fullName')
        .exec(function (err, article) {
            if (err) return next(err);
            if (!article) return next(new Error('Failed to load article ' + id));

            req.article = article;
            next();
        });
};

exports.read = function (req, res) {
    res.json(req.article);
};

exports.update = function (req, res) {
    var article = req.article;

    article.title = req.body.title;
    article.content = req.body.content;

    article.save(function (err) {
        if (err) {
            return res.status(400).send({ message: getErrorMessage(err) });
        } else {
            res.json(article);
        }
    });
};

exports.delete = function (req, res) {
    var article = req.article;

    article.remove(function (err) {
        if (err) {
            return res.status(400).send({ message: getErrorMessage(err) });
        } else {
            res.json(article);
        }
    });
};

//Checking an Authorization.
exports.hasAuthorization = function (req, res, next) {
    if (req.article.creator.id !== req.user.id) {
        return res.status(403).send({ message: '작성자 본인만 가능합니다.' });
    }

    next();
};