/**
 * Created by Jack on 12/19/16.
 */
var mongoose = require('mongoose'),
	Match = mongoose.model('Match');

var getErrorMessage = function (err) {
	if (err.errors) {
		for (var errName in err.errors) {
			if (err.errors[errName].message) return err.errors[errName].message;
		}
	} else {
		return '알 수 없는 서버 에러';
	}
};

var currentMonth = new Date().getMonth() + 1;
	
exports.monthlyMatchList = function (req, res) {
	var monthQuery = {};
	monthQuery['month'] = currentMonth;
	
	Match.find(monthQuery).sort('index').exec(function (err, matches) {
		if (err) {
			return res.status(400).send({ message: getErrorMessage(err) });
		} else {
			console.log(matches);
			res.json(matches);
		}
	});
};