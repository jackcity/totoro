/**
 * Created by Jack on 9/23/16.
 */
var User = require('mongoose').model('User'),
    passport = require('passport');

exports.signup = function (req, res, next) {
    if (!req.user) {
        console.log('req.body : ', req.body);
        var user = new User(req.body);

        user.coin = 50000;
        user.provider = 'local';

        user.save(function (err) {
            if (err) {
                console.log(err);
                return res.redirect('/signup');
            }
            req.login(user, function (err) {
                if (err) return next(err);

                return res.redirect('/');
            });
        });
    } else {
        return res.redirect('/');
    }
};

exports.logout = function (req, res) {
    req.logout();
    res.redirect('/');
};

//Checking an Authentication.
exports.requiresLogin = function (req, res, next) {
    if (!req.isAuthenticated()) {
        return res.status(401).send({ message: '로그인이 필요합니다.' });
    }

    next();
};