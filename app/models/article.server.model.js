/**
 * Created by Jack on 9/29/16.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ArticleSchema = new Schema({
    created: {
        type: Date,
        default: Date.now,
        required: true
    },
    title: {
        type: String,
        default: '',
        trim: true,
        required: true
    },
    content: {
        type: String,
        default: '',
        trim: true
    },
    creator: {
        type: Schema.ObjectId,
        ref: 'User',
	    required: true
    },
    file: {
        type: String,
        default: '',
        trim: true
    }
});

mongoose.model('Article', ArticleSchema);