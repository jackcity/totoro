/**
 * Created by Jack on 11/25/16.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var MatchSchema = new Schema({
	awayTeamWin: {
		type: Boolean,
		default: false
	},
	awayTeam: {
		type: String,
		default: '',
		trim: true
	},
	awayScore: {
		type: String,
		default: '',
		trim: true
	},
	state: {
		type: String,
		default: '',
		trim: true
	},
	homeScore: {
		type: String,
		default: '',
		trim: true
	},
	homeTeam: {
		type: String,
		default: '',
		trim: true
	},
	homeTeamWin: {
		type: Boolean,
		default: false
	},
	place: {
		type: String,
		default: '',
		trim: true
	},
	time: {
		type: String,
		default: '',
		trim: true
	},
	date: {
		type: String,
		default: '',
		trim: true
	},
	month: {
		type: Number,
		default: 0
	},
	season: {
		type: String,
		default: '',
		trim: true
	},
	index: {
		type: Number,
		default: 0
	}
});

mongoose.model('Match', MatchSchema);