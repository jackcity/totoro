/**
 * Created by Jack on 9/22/16.
 */
var mongoose = require('mongoose'),
    crypto = require('crypto'),
    Schema = mongoose.Schema;

var UserSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true,
	    trim: true
    },
    password: {
    	type: String,
	    validate: [
	    	function (password) {
    		    return password.length >= 10;
		    },
		    '비밀번호는 최소 열자 이상이어야 합니다.'
	    ]
    },
	salt: String,
	role: {
    	type: String,
		required: true,
		enum: ['Admin', 'User']
	},
    fullName: {
    	type: String,
	    required: true
    },
    email: {
    	type: String,
	    match: [
	    	/.+\@.+\..+/,
		    '올바른 이메일 형식으로 입력해 주세요.'
	    ]
    },
    coin: Number,
	betData: [{
    	type: Schema.ObjectId,
		ref: 'Bet'
	}],
    provider: String,
    providerId: String,
    providerData: {},
    created: {
        type: Date,
        default: Date.now,
	    required: true
    }
});

UserSchema.pre('save', function (next) {
    if (this.password) {
    	this.salt = new Buffer(crypto.randomBytes(256).toString('base64'), 'base64');
        this.password = this.hashPassword(this.password);
    }
    next();
});

UserSchema.methods.hashPassword = function (password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 100000, 512, 'sha512');

    return hash.toString('base64');
};

UserSchema.methods.authenticate = function (password) {
    return this.password === this.hashPassword(password);
};

UserSchema.set('toJSON', {
    getters: true,
    virtuals: true
});

mongoose.model('User', UserSchema);