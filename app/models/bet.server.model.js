/**
 * Created by office on 2016-10-19.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var BetSchema = new Schema({
	betDate: {
		type: Date,
		default: Date.now,
		required: true
	},
	matchData: {
		type: Schema.ObjectId,
		ref: 'Match',
		required: true
	},
	betCoin: {
		type: Number,
		required: true
	},
	betResult: {
		type: String,
		default: 'waiting',
		required: true,
		enum: ['win', 'lose', 'waiting']
	}
});

mongoose.model('Bet', BetSchema);