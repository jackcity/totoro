/**
 * Created by Jack on 9/29/16.
 */
var usersCtrl = require('../controllers/users.server.controller.js'),
    articlesCtrl = require('../controllers/articles.server.controller.js');

module.exports = function (app) {
    app.route('/api/articles')
        .get(articlesCtrl.list)
        .post(usersCtrl.requiresLogin, articlesCtrl.create);

    app.route('/api/articles/upload')
        .post(articlesCtrl.uploadTest);
    
    app.route('/api/articles/:articleId')
        .get(articlesCtrl.read)
        .put(usersCtrl.requiresLogin, articlesCtrl.hasAuthorization, articlesCtrl.update)
        .delete(usersCtrl.requiresLogin, articlesCtrl.hasAuthorization, articlesCtrl.delete);

    app.param('articleId', articlesCtrl.articleByID);
};