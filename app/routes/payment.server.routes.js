/**
 * Created by office on 2016-10-28.
 */
var paymentCtrl = require('../controllers/payment.server.controller.js');

module.exports = function (app) {
    app.route('/api/payment')
        .post(paymentCtrl.acceptPayment);
};