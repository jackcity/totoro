/**
 * Created by Jack on 12/19/16.
 */
var matchesCtrl = require('../controllers/matches.server.controller.js');

module.exports = function (app) {
	app.route('/api/matches/monthly')
		.get(matchesCtrl.monthlyMatchList);
};