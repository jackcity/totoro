/**
 * Created by Jack on 1/9/17.
 */
var adminCtrl = require('../controllers/admin.server.controller.js');

module.exports = function (app) {
	app.route('/admin/match/remove')
		.post(adminCtrl.removeMatch);
	
	app.route('/admin/match/save')
		.post(adminCtrl.saveMatch);
};