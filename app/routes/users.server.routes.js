/**
 * Created by Jack on 9/23/16.
 */
var usersCtrl = require('../controllers/users.server.controller.js'),
    passport = require('passport');

module.exports = function (app) {
    app.route('/signup')
        .post(usersCtrl.signup);

    app.route('/login')
        .post(passport.authenticate('local', {
            successRedirect: '/',
            failureRedirect: '/login'
        }));

    app.get('/logout', usersCtrl.logout);
};