/**
 * Created by Jack on 9/19/16.
 */
module.exports = function (app) {
    var indexCtrl = require('../controllers/index.server.controller.js');

    app.get('/', indexCtrl.render);
};