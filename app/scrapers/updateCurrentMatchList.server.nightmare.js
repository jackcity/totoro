/**
 * Created by Jack on 12/10/16.
 */
require('../models/match.server.model.js');

var Promise = require('bluebird'),
	Nightmare = require('nightmare'),
	nightmare = Nightmare(),
	mongoose = require('mongoose'),
	Match = mongoose.model('Match');

var getMatchListFromSite = function () {
	var matches = [];
	
	for (var i = 0; i < document.querySelectorAll('#_monthlyScheduleList tr').length; i++) {
		var matchObj = {
			index: 0,
			season: '',
			month: 0,
			date: '',
			time: '',
			place: '',
			homeTeamWin: false,
			homeTeam: '',
			homeScore: '',
			state: '',
			awayScore: '',
			awayTeam: '',
			awayTeamWin: ''
		};
		var season = document.querySelector('#_seasonSelect span._seasonTitle'),
			month = document.querySelector('div.schedule_month li.selected span.month').innerText,
			date = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('th div.inner'),
			time = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('span.time'),
			place = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('span.place'),
			homeTeamWin = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('span.team_left span.win'),
			homeTeam = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('span.team_left span.name'),
			homeScore = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('span.team_left span.score'),
			state = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('td.game_content span'),
			awayScore = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('span.team_right span.score'),
			awayTeam = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('span.team_right span.name'),
			awayTeamWin = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('span.team_right span.win');
		
		matchObj.index = i;
		matchObj.season = season.innerText;
		if (date) {
			matchObj.date = date.innerText;
		} else {
			matchObj.date = '';
		}
		matchObj.month = Number(month.slice(0, -1));
		if (time) {
			matchObj.time = time.innerText;
			matchObj.place = place.innerText;
			matchObj.homeTeamWin = homeTeamWin ? true : false;
			matchObj.homeTeam = homeTeam.innerText;
			matchObj.state = state.innerText;
			matchObj.awayTeam = awayTeam.innerText;
			matchObj.awayTeamWin = awayTeamWin ? true : false;
			if (homeScore) {
				matchObj.homeScore = homeScore.innerText;
				matchObj.awayScore = awayScore.innerText;
			}
		} else {
			matchObj.state = '경기가 없습니다';
		}
		
		matches.push(matchObj);
	}
	
	return matches;
};

var currentYear = new Date().getFullYear(),
	currentMonth = new Date().getMonth() + 1,
	currentDate = new Date().getDate(),
	currentHour = new Date().getHours(),
	currentMinute = new Date().getMinutes();

if (currentMonth === 1 && currentDate === 1 && currentHour < 2) {
	if (currentMinute % 2 === 0) {
		currentYear -= 1;
		currentMonth = 12;
	}
} else if (currentMonth > 1 && currentDate === 1 && currentHour < 2) {
	if (currentMinute % 2 === 1) {
		currentMonth -= 1;
	}
}

var uri = 'http://sports.naver.com/wfootball/schedule/index.nhn?category=epl&year=' + currentYear + '&month=' + currentMonth,
	siteMatchList = nightmare.goto(uri).wait('#_monthlyScheduleList').evaluate(getMatchListFromSite);

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/webserver-basic');

var modifyMatches = function (siteMatches, dbMatches) {
	var modifiedMatches = [];
	// for (var i = 0; i < siteMatches.length; i++) {
	for (var i = 0; i < 31; i++) {
		if (siteMatches[i].state !== dbMatches[i].state) {
			dbMatches[i].homeTeamWin = siteMatches[i].homeTeamWin;
			dbMatches[i].homeScore = siteMatches[i].homeScore;
			dbMatches[i].state = siteMatches[i].state;
			dbMatches[i].awayScore = siteMatches[i].awayScore;
			dbMatches[i].awayTeamWin = siteMatches[i].awayTeamWin;
			
			modifiedMatches.push(dbMatches[i]);
		}
		/*else {
		 console.log('Caution!!! (Index[' + i + ']: Both of the matches are definitely same. Nothing to update...');
		 }*/
	}
	
	return modifiedMatches;
};

var findMatches = function (results) {
	var foundMatches;
	for (var i = 0; i < results.length; i++) {
		foundMatches =  Match.findById(results[i]._id).exec(function (err, match) {
			if (err) console.log(err);
			else console.log(match._id);
		});
	}
	
	return foundMatches;
};

var updateMatches = function (results) {
	var updatedMatches;
	for (var i = 0; i < results.length; i++) {
		updatedMatches = new Promise(function (resolve, reject) {
			Match.findByIdAndUpdate(results[i]._id, results[i], { new: true }).exec(function (err, match) {
				if (err) {
					reject(err);
				} else {
					console.log('updated matches : ', match);
					resolve(match);
				}
			});
		});
	}
	
	return updatedMatches;
};

Promise.all([
	new Promise(function (resolve, reject) {
		resolve(siteMatchList);
	}),
	new Promise(function (resolve, reject) {
		var dbMatchList = [];
		
		Match.find({ 'month': currentMonth }).exec(function (err, matches) {
			if (err) {
				reject(err);
			}
			else {
				dbMatchList = matches;
				
				resolve(dbMatchList);
			}
		});
	})
]).spread(function (siteMatches, dbMatches) {
	// Have to make it to run again if there's an error.
	if (siteMatches[0].month !== dbMatches[0].month) {
		console.log('Error!!! : The siteMatches\' month and the dbMatches\' one are not same.');
	} else {
		console.log(dbMatches[0]);
		if (siteMatches.length !== dbMatches.length) {
			console.log('Error!!! : The schedule might be changed. Check it out and update a monthly match list to MongoDB again.');
		} else {
			return Promise.all(modifyMatches(siteMatches, dbMatches)).then(function (modifiedMatches) {
				return findMatches(modifiedMatches);
			});
		}
	}
}).catch(function (err) {
	console.log(err);
}).finally(function () {
	process.exit(0);
});
