/**
 * Created by Jack on 12/8/16.
 */
// Execute this single file for saving a monthly match list into the mongodb.
// It should be executed at the end of the month because a match schedule is changed sometimes.
require('../models/match.server.model.js')

var Nightmare = require('nightmare'),
	nightmare = Nightmare(),    // Seeing Electron window option is "{ show: true }".
	mongoose = require('mongoose'),
	Match = mongoose.model('Match');
var adminCtrl = require('../controllers/admin.server.controller.js');
	year = adminCtrl.year,
	month = adminCtrl.month,
	uri = 'http://sports.naver.com/wfootball/schedule/index.nhn?category=epl&year=' + year + '&month=' + month;

var getMatchList = function () {
	var matches = [];
	
	for (var i = 0; i < document.querySelectorAll('#_monthlyScheduleList tr').length; i++) {
		var matchObj = {
			index: 0,
			season: '',
			month: 0,
			date: '',
			time: '',
			place: '',
			homeTeamWin: false,
			homeTeam: '',
			homeScore: '',
			state: '',
			awayScore: '',
			awayTeam: '',
			awayTeamWin: false
		};
		var season = document.querySelector('#_seasonSelect span._seasonTitle'),
			month = document.querySelector('div.schedule_month li.selected span.month').innerText,
			date = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('th div.inner'),
			time = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('span.time'),
			place = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('span.place'),
			homeTeamWin = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('span.team_left span.win'),
			homeTeam = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('span.team_left span.name'),
			homeScore = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('span.team_left span.score'),
			state = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('td.game_content span'),
			awayScore = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('span.team_right span.score'),
			awayTeam = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('span.team_right span.name'),
			awayTeamWin = document.querySelectorAll('#_monthlyScheduleList tr')[i].querySelector('span.team_right span.win');
		
		matchObj.index = i;
		matchObj.season = season.innerText;
		if (date) {
			matchObj.date = date.innerText;
		} else {
			matchObj.date = '';
		}
		matchObj.month = Number(month.slice(0, -1));
		if (time) {
			matchObj.time = time.innerText;
			matchObj.place = place.innerText;
			matchObj.homeTeamWin = homeTeamWin ? true : false;
			matchObj.homeTeam = homeTeam.innerText;
			matchObj.state = state.innerText;
			matchObj.awayTeam = awayTeam.innerText;
			matchObj.awayTeamWin = awayTeamWin ? true : false;
			if (homeScore) {
				matchObj.homeScore = homeScore.innerText;
				matchObj.awayScore = awayScore.innerText;
			}
		} else {
			matchObj.state = '경기가 없습니다';
		}
		
		matches.push(matchObj);
		// matches['match' + (i + 1)] = matchObj;
	}
	
	return matches;
};

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/webserver-basic');

nightmare
	.goto(uri)
	.wait('#footer')
	.evaluate(getMatchList)
	.end()
	.then(function (matches) {
		for (var i = 0; i < matches.length; i++) {
			var match = new Match(matches[i]);

			match.save(function (err) {
				if (err) throw err;
			});
		}
		console.log('It\'s done to save...');
	})
	.catch(function (err) {
		console.log('failed : ' + err);
	});